#!/usr/bin/env python3

#--url "https://mainnet.infura.io/v3/YOUR_INFURA_ID" --file "ethbal.json"

"""
Input file format:

balances.json
{
  "sourceWallet": [
    {
      "address": "wallet_address_1"
    },
    {
      "address": "wallet_address_2"
    },

    ...

    {
      "address": "wallet_address_n"
    }
  ]
}

"""

import argparse
import os
import os.path
import sys
import json
from prettytable import PrettyTable
from web3 import Web3


def read_wallets(file_name):
    """
    Reads JSON file with source and destination wallets.

    Args:
        :param rpc: blockchain RCP URL.
        :param addr: wallet update.

    Returns:
        :return: balance of wallet.
    """
    if os.path.isfile(file_name):
        print("Processing: {}".format(file_name))
        with open(file_name, 'r') as json_file:
            json_data = json.load(json_file)
            return json_data
    else:
        print("File: {} does not exist. Exiting.".format(file_name))
        sys.exit()


def get_address_balance(rpc, addr):
    """
    Retrieves wallet balance from blockchain.

    Args:
        :param rpc: blockchain RCP URL.
        :param addr: wallet update.

    Returns:
        :return: balance of wallet.
    """
    web3 = Web3(Web3.HTTPProvider(rpc))
    balance = web3.eth.getBalance(addr)
    return balance


def main():
    """
    Main function runs appropriate action depends on passed parameters.

    Returns:
        :return: nothing is returned from main function.
    """

    cmdparser = argparse.ArgumentParser()
    cmdparser.version = "0.2"
    cmdparser.add_argument("-u", "--url", type=str, action="store", dest="url", help="blockchain rpc url")
    cmdparser.add_argument("-f", "--file", type=str, action="store", dest="file",  help="specifies input file")
    cmdparser.add_argument("-v", "--version",  action="version")
    args = cmdparser.parse_args()

    if args.file:
        input_file = args.file
    else:
        input_file = "{}/balances.json".format(os.path.dirname(os.path.realpath(__file__)))

    if args.url:
        rpcURL = args.url
    else:
        rpcURL = 'https://mainnet.infura.io/v3/YOUR_INFURA_ID'

    data = read_wallets(input_file)

    balance_table = PrettyTable()
    balance_table.field_names = ["address", "balance"]

    for wallet in data["sourceWallet"]:
        balance = get_address_balance(rpcURL, wallet["address"])
        balance_table.add_row([
            wallet['address'],
            Web3.fromWei(balance, 'ether')
        ])

    print(balance_table)

main()