# eth-balance

First Python script, which pulls data from ETH-based blockchain.

Files included in project:
 - **eth_balance.py** - script which pulls data from blockchain
 - **balances.json** - default file which contains list of wallets



## eth_balance.py
Script opens JSON file with list of wallets and uses RPC API to pull balances from blockchain.

It can be used with any eth-based blockchain.

#### Parameters
 - **-u** or **--url** - specifies URL to RPC blockchain API
 - **-f** or **--file** - specifies path to JSON file with list of wallets
 - **-v** or **--version** - displays script version

If there is no URL parameter specified, script will use default URL from **rpcURL** variable in script.
That variable should contain default URL to blockchain API which is publibly accessible.

If there is no filename specified with list fo wallets, script will default to **balances.json**.


## balances.json
Default file which will be opened, if no filename with list of wallets is specified when running script.

File contains list of wallets for script to pull balances from.

Structure of file:
```json
{
  "sourceWallet": [
    {
      "address": "0x0A..."
    },
    {
      "address": "0x38..."
    }
  ]
}
```


## License
MIT / BSD


## Donations
If you found some value and would like ot support this and future projects you can donate:

**BTC** 3LX9cdyXvbAdhUnCteKVtS7uxQz2mqeLfB  
**LTC** MHKgdcZxcyC937otEqwMWvSPANmRVXdbyC  
**ETH** 0xe2D5c108C8219f97d7a3a91558ff11138eEd2814  
**DGB** DJgNTXkyG5u6rujVMZFQDPtf7ueSgAomNw  
**BCH** qz4lm4fxn8tvrze33afr85sfrz73uz5vwcycyngphk 